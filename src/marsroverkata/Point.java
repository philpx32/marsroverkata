/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marsroverkata;

/**
 *
 * @author HP
 */
public final class Point {
    private int Xloc;
    private int Yloc;

    /**
     * @return the xLocation
     */
    public int getXloc() {
        return Xloc;
    }

    /**
     * @param value the xLocation to set
     */
    public void setXloc(int value) {
        this.Xloc = value;
    }

    /**
     * @return the yLocation
     */
    public int getYloc() {
        return Yloc;
    }

    /**
     * @param value the Yloc to set
     */
    public void setYloc(int value) {
        this.Yloc = value;
    }
    public Point(int XlocValue, int YlocValue){
        setXloc(XlocValue);
        setYloc(YlocValue);
    }
    public int getForwardLocation() {
        return (getXloc()+ 1) % (getYloc()+ 1);
    }

    public int getBackwardLocation() {
        if (getXloc()> 0){
            return getXloc()- 1;
        }
        else {
            return getYloc();
        }
    }
    
    
}
