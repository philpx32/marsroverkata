/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marsroverkata;

/**
 *
 * @author HP
 */
public final class MarsRoverKata {
    private Coordinates coordinates;

    /**
     * @return the coordinates
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * @param value the coordinates to set
     */
    public void setCoordinates(Coordinates value) {
        this.coordinates = value;
    }
    
    public MarsRoverKata(Coordinates cValue){
        setCoordinates(cValue);
    }
    //receive commands to move forward/backward
    public void receiveCommands(String commands){
        try{
        for (char command : commands.toCharArray()) {
            if (!receiveSingleCommand(command)) {
                break;
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    //receive single command to move forward/backward
    public boolean receiveSingleCommand(char command) throws Exception {
        switch(Character.toLowerCase(command)) {
            //commands to move rover forward/backward
            case 'f':
                return getCoordinates().moveForward();
            case 'b':
                return getCoordinates().moveBackward();
            // commands to turn the rover left/right
            case 'l':
                getCoordinates().changeDirectionLeft();
                return true;
            case 'r':
                getCoordinates().changeDirectionRight();
                return true;
            default:
                throw new Exception("Unknown Command " + command);
        }
    }
    public String getPosition() {
        return getCoordinates().toString();
    }
    
}
