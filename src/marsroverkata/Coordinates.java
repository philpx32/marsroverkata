/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marsroverkata;

/**
 *
 * @author HP
 */
public final class Coordinates {
    private Point x;
    private Point y;
    private Direction direction;

    /**
     * @return the x
     */
    public Point getX() {
        return x;
    }

    /**
     * @param value the x to set
     */
    public void setX(Point value) {
        this.x = value;
    }

    /**
     * @return the y
     */
    public Point getY() {
        return y;
    }

    /**
     * @param value the y to set
     */
    public void setY(Point value) {
        this.y = value;
    }

    /**
     * @return the direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * @param value the direction to set
     */
    public void setDirection(Direction value) {
        this.direction = value;
    }
    public Coordinates(Point xValue,
            Point yValue,
            Direction directionValue) {
        setX(xValue);
        setY(yValue);
        setDirection(directionValue);
    }
    protected boolean move(Direction directionValue) {
        int xLocation = x.getXloc();
        int yLocation = y.getYloc();
        switch (directionValue) {
            case NORTH:
                yLocation = y.getForwardLocation();
                break;
            case EAST:
                xLocation = x.getForwardLocation();
                break;
            case SOUTH:
                yLocation = y.getBackwardLocation();
                break;
            case WEST:
                xLocation = x.getBackwardLocation();
                break;
        }
        x.setXloc(xLocation);
        y.setYloc(yLocation);
        return true;
    }

    public boolean moveForward() {
        return move(direction);
    }

    public boolean moveBackward() {
        return move(direction.getBackwardDirection());
    }
    //to change direction of rover -1/left 1/right
    private void changeDirection(Direction aDirection, int turnKey) {
        int directions = Direction.values().length;
        int index = (directions + aDirection.getValue() + turnKey) % directions;
        direction = Direction.values()[index];
    }

    public void changeDirectionLeft() {
        changeDirection(direction, -1);
    }

    public void changeDirectionRight() {
        changeDirection(direction, 1);
    }
    
    
}
