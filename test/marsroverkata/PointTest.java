/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marsroverkata;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

/**
 *
 * @author HP
 */
public class PointTest {
    Point point;
    private final int xloc = 3;
    private final int yloc = 7;
    
    
    @Before
    public void setUp() {
        point = new Point(xloc,yloc);
    }
    
    /**
     * Test of new instance, of class Point.
     */
    @Test
    public void newInstanceShouldSetXlocationAndYLocationParams() {
        assertThat(point.getXloc()).isEqualTo(xloc);
        assertThat(point.getYloc()).isEqualTo(yloc);
    }
    
    
}
