/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marsroverkata;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

/**
 *
 * @author HP
 */
public class CoordinatesTest {
        private Coordinates coordinates;
    private Point x;
    private Point y;
    private final Direction direction = Direction.NORTH;
    
    @Before
    public void setUp() {
        x = new Point(1, 50);
        y = new Point(2, 50);
        coordinates = new Coordinates(x, y, direction); 
    }
    

    /**
     * Test of new instance of class Coordinates.
     */
    @Test
    public void newInstanceShouldSetXAndYParams() {
        assertThat(coordinates.getX()).isEqualToComparingFieldByField(x);
        assertThat(coordinates.getY()).isEqualToComparingFieldByField(y);
    }
    
}
