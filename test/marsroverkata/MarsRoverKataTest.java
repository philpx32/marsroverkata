/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marsroverkata;

import org.junit.Before;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

/**
 *
 * @author HP
 */
public class MarsRoverKataTest {

    private MarsRoverKata aMarsRoverKata;
    private Coordinates aCoordinate;
    private final Direction aDirection = Direction.NORTH;
    private Point x;
    private Point y;

    @Before
    public void setUp() {
        x = new Point(3, 6);
        y = new Point(4, 6);
        aCoordinate = new Coordinates(x, y, aDirection);
        aMarsRoverKata = new MarsRoverKata(aCoordinate);
    }

    //test whether rover coordinates and direction are being set
    @Test
    public void newInstanceShouldSetRoverCoordinatesAndDirection() {
        try{
        assertThat(aMarsRoverKata.getCoordinates()).isEqualToComparingFieldByField(aCoordinate);
        } catch (Exception e) {

        }
    }

    //is rover able to receive single command and move forward
    @Test
    public void receiveSingleCommandShouldMoveForwardWhenCommandIsF() {
        try {
            int expected = y.getXloc() + 1;
            aMarsRoverKata.receiveSingleCommand('f');
            assertThat(aMarsRoverKata.getCoordinates().getY().getYloc()).isEqualTo(expected);
        } catch (Exception e) {

        }
    }

    //is rover able to receive single command and move backward
    @Test
    public void receiveSingleCommandShouldMoveBackwardWhenCommandIsB(){
        try {
        int expected = y.getXloc() - 1;
        aMarsRoverKata.receiveSingleCommand('b');
        assertThat(aMarsRoverKata.getCoordinates().getY().getYloc()).isEqualTo(expected);
        } catch (Exception e) {

        }
    }

    //turn rover left
    @Test
    public void receiveSingleCommandShouldTurnLeftWhenCommandIsL(){
        try {
        aMarsRoverKata.receiveSingleCommand('l');
        assertThat(aMarsRoverKata.getCoordinates().getDirection()).isEqualTo(Direction.WEST);
        } catch (Exception e) {

        }
    }

    //turn right
    @Test
    public void receiveSingleCommandShouldTurnRightWhenCommandIsR(){
       try {
        aMarsRoverKata.receiveSingleCommand('r');
        assertThat(aMarsRoverKata.getCoordinates().getDirection()).isEqualTo(Direction.EAST);
        } catch (Exception e) {

        }
    }

    @Test
    public void receiveSingleCommandShouldIgnoreCase(){
        try {
        aMarsRoverKata.receiveSingleCommand('R');
        assertThat(aMarsRoverKata.getCoordinates().getDirection()).isEqualTo(Direction.EAST);
        } catch (Exception e) {

        }
    }

    @Test
    public void receiveCommandsShouldBeAbleToReceiveMultipleCommands(){
        try {
        int expected = x.getXloc() + 1;
        aMarsRoverKata.receiveCommands("RFR");
        assertThat(aMarsRoverKata.getCoordinates().getX().getXloc()).isEqualTo(expected);
        assertThat(aMarsRoverKata.getCoordinates().getDirection()).isEqualTo(Direction.SOUTH);
        } catch (Exception e) {

        }
    }

    @Test
    public void receiveCommandShouldWrapFromOneEdgeOfTheGridToAnother(){
        try {
        int expected = x.getYloc() + x.getXloc() - 3;
        aMarsRoverKata.receiveCommands("LFF");
        assertThat(aMarsRoverKata.getCoordinates().getX().getYloc()).isEqualTo(expected);
        } catch (Exception e) {

        }
    }

}
